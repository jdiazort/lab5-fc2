#ifndef PUZZLE_H
#define PUZZLE_H

#include <iostream>
#include <vector>	// for vector
#include <string>	// for strings
#include <fstream>	// for ifstream

using namespace std;

template< typename T >		// unsing templating to create Puzzle class
class Puzzle {

  public:
	Puzzle(int d=9);		// constructor that defaults the value of the dimensions to 9
	void playGame();		// function that implements the Sudoku game

  private:						// helper funtions
	void showBoard();			// formats the board and prints it
	void placeValue(int,int,T);	// places a value in a cell in the board
	bool boardIsFull();			// checks if the board is full, does not have any 0's in it
	bool insideBoard(int,int);	// checks if the value iserted by the user is within the boundaries of the board
	bool insideRange(T);		// checks if the inserted value is in the range [1,9] (from 1 to 9)
	bool possibleMove(int,int,T); // checks if the value assigned to a cell is valid, given the other values in the board
	
	int boardDimensions;		// keeps track of the dimensions of the board, initialized to 9 in the constructor
	vector<vector <T> >  board; 

};

// Constructor for the sudoku game. It sets the value of the dimensions of the board to 9 and takes the input file from the user (the sudoku board values) and saves them in a 2 dimensional array. Then it calls the showBoard() function to 
template< typename T>
Puzzle< T >::Puzzle(int d)
			: boardDimensions(d)
{
	string fileName;
	int value;
	cout << "Please enter a file name: " ;
	cin >> fileName;

	ifstream inFile;
	inFile.open(fileName.c_str());

	vector<T> tempVec;
	T tempVar;

	while (!inFile.eof()) {	 // While the file is not at the end 
		for (int i=0; i<boardDimensions; i++){	// Read in a line into a temporary vector
				inFile >> tempVar;	// Put just read value into temporary vector
				tempVec.push_back(tempVar);
			}
		board.push_back(tempVec);   // Push back the line just read)
		tempVec.clear(); // Clear/recycle temporary vector
	}

	showBoard();

}

// Public function that is in charge of actually playing the sudoku game. While the board is not full, it will ask users for new x and y coordinates of a cell where they would place a new value. It does not check whether an input is valid, but calls the palceValue function, which is in charge of calling all the other helper functions that perfrom the error checking, and is also responsible of asking the user again for new inputs (in a recursive manner) until they input valid values for the corrdinates and the cell value. After the board is full, it displays a WIN message to promt the user of the end of the game, and that he has solved the sudoku.
template< typename T >
void Puzzle<T>::playGame() {
	int x;
	int y;
	T value;
	cout << "Welcome to sudoku game! \nInstructions:"<< endl;
	cout << "In order to place a value in a cell, enter an integer between 1-9 for the x and y coordinates" << endl;
	while (!boardIsFull()){
		showBoard();
		cout<<"Please enter the location where you want to play and specify the value you wish to place. " << endl; 
		cout << "X coordinate: ";
		cin >> x;
		cout << "Y coordinate: ";
		cin >> y;
		cout << "Value: ";
		cin >> value;
		placeValue(x,y,value);
		cout << "Valid Input!" << endl;
	}
	showBoard();
	cout << "\nYOU WIN! YOU SOLVED THE SUDOKU PUZZLE\n\n" << endl;
	
}


// Private helper function that displays the board. It divides the board into 9 mini-boxes, and also labels and numbers the x and y axis.
template< typename T>
void Puzzle< T >::showBoard() {
	int lineCount=1;
	int squareCount=0;
	cout << "\\ y 1 2 3 | 4 5 6 | 7 8 9 |" << endl;
	cout << "x -------------------------\n";
	for (int i=0; i<boardDimensions; i++){
		cout << i+1 <<" | ";
		for (int j=0; j<boardDimensions; j++) {
			if (lineCount%3==0)
				cout << board[i][j] << " | " ;
			else
				cout<<  board[i][j] << " " ;
			lineCount++;
		}
	squareCount++;
	cout << "\n";
	if (squareCount%3==0)
		cout << "  -------------------------\n";
	}
}

// Private helper function that places a value in a cell, specified by the x and y coordinates of that cell. The function uses recurssion to make sure that the inserted value is a valid one: within the dimensions of the board,  within the 1-9 range, and a posssible move given the current status of the board (given the values of the other cells). It does so by calling other helper functions that perform these checks.
template< typename T >
void Puzzle< T >::placeValue(int x, int y, T value) {
	if ( insideBoard(x,y) && insideRange(value) && possibleMove(x-1,y-1, value) ){
		board[x-1][y-1]=value;
	}
	else {
		cout << "Invalid value. Please enter the cell location and value again. " << endl;
		cout << "X coordinate: ";
		cin >> x;
		cout << "Y coordinate: ";
		cin >> y;
		cout << "Value: ";
		cin >> value;
		placeValue(x,y,value);
	}
}

// Private helper function that checks whether a specified cell (denoted by x and y coordinates, is within the confines of the board. It checks whether the values are in the range between 1 and the dimensions private data memeber of the board. If so, returns 1, 0 otherwise. 
template< typename T >
bool Puzzle< T >::insideBoard(int x, int y) {
	if ( (x<=boardDimensions && x>0) && (y<=boardDimensions && y>0) )
		return 1;
	else 
		return 0;
}

// Private helper function that checks whether the inserted integer valur is in the range 1-(value of board dimensions). This value is usually 9. If so, returns 1, 0 othewise.
template< typename T >
bool Puzzle< T >::insideRange(T value) {
	if (value>0 && value<=boardDimensions)
		return 1;
	else
		return 0;
}

// Private helper function that checks whether the board is full. Return 1 if is full, 0 otherwise. It checks if there is a 0 inside the board. If not, it determines that the board is full. This function is called every time a new cell is successfully assigned a new value. If so, returns 1, 0 othewise.
template< typename T >
bool Puzzle< T >::boardIsFull() {
	for (int i=0; i<boardDimensions; i++){
		for (int j=0; j<boardDimensions; j++) {
			if (board[i][j]==0)
				return 0;
		}
	}
	return 1;
}



// Private helper function that checks whether an input is valid according to the current state of the board (the current values of the other cells in the board. First, the function checks if there is already a value (other than zero) in the cell where the user is trying to place a new value. The function first checks if the new value is already horizontally in the board. Then it checks whether the value is veritacally in the board. And finally, it checks whether the value is already in the mini-cell where it is trying to be placed. If the input is valid, it returns true, false otherwise.
template< typename T> 
bool Puzzle<T>::possibleMove(int x, int y, T value) {
	
	if (board[x][y]!=0)				// checks if there is already a  value in the cell where the user wants to play in
		return false;
	
	// check horizontally
	for (int i=0; i<y; i++) { // checks from the begining of the row, up until the cell that the user is trying to set a new value (horizontally)
		if ( value == board[x][i] )
			return false;
	}
	for (int j=y+1; j<boardDimensions; j++){ // checks from after the cell the user is trying to assign a value (horizontally), up until the end of the row 
		if ( value == board[x][j] )
			return false;
	}

	// check vertically 
	for (int i=0; i<x; i++) { // checks from the begining of the column, up until the cell that the user is trying to set a new value (vertically)
		if ( value == board[i][y] )
			return false;
	}
	for (int j=x+1; j<boardDimensions; j++){ // checks from after the cell the user is trying to assign a value (vertically), up until the end of the column 
		if ( value == board[j][y] )
			return false;
	}
	// check mini-box
	int relativeX = x/3;
	int relativeY = y/3;
	// parses through all the cells in the mini-cell and checks if the value is already among the cells of the mini-box with nested for loops
	for (int i=relativeX*3; i<((relativeX*3)+3); i++){ 
		for (int j=relativeY*3; j<((relativeY*3)+3); j++){
			if (!(x==i && y==j)){	// makes sure the compared cell is not the one we are trying to set a value to
				if (value==board[i][j])
					return false;
			}
		}
	}

	return true; 	// if the cell value is valid, it returns true
	
}


#endif
