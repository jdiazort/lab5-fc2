
#include <stdio.h>	// for printf
using namespace std; 

#include "Puzzle.h" // declaration of Mortgage class

int main() {
	cout << "\nTesting out Templating: input wordoky.txt to show board with characters. " << endl;
	Puzzle<char> wordoky;
	cout << "\nTesting out Sudoku Game: input sudoku.txt to show board with integers. " << endl;
	Puzzle< int > sudoku;		// initialization of a board in which connect4 will be played
	cout << "\nStarting Sudoku Game ... \n " << endl;
	sudoku.playGame();
} // end main function
